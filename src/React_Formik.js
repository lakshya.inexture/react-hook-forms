import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";

function Formik() {
    const formik = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            email: "",
        },
        validationSchema: Yup.object({
            firstName: Yup.string()
                .required("Please enter your first name")
                .matches(/[a-zA-Z]{3}/, "Please enter text only"),
            lastName: Yup.string()
                .required("Please enter your last name")
                .matches(/[a-zA-Z]{3}/, "Please enter text only"),
            email: Yup.string()
                .required("Please enter your email")
                .matches(
                    /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                    "Please enter a valid email address"
                ),
        }),
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });
    return (
        <div className="container formik-wrapper">
            <form onSubmit={formik.handleSubmit}>
                <input
                    name="firstName"
                    id="firstName"
                    type="text"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.firstName}
                    placeholder="First Name"
                />
                <p>
                    {formik.touched.firstName && formik.errors.firstName
                        ? formik.errors.firstName
                        : null}
                </p>
                <input
                    type="text"
                    name="lastName"
                    id="lastName"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.lastName}
                    placeholder="Last Name"
                />
                <p>
                    {formik.touched.lastName && formik.errors.lastName
                        ? formik.errors.lastName
                        : null}
                </p>
                <input
                    type="email"
                    name="email"
                    id="email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                    placeholder="Email"
                />
                <p>
                    {formik.touched.email && formik.errors.email
                        ? formik.errors.email
                        : null}
                </p>
                <button type="submit" className="btn btn-dark">
                    Submit
                </button>
            </form>
        </div>
    );
}

export default Formik;
