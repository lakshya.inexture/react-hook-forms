import React from "react";
import { useForm } from "react-hook-form";
import "./App.css";

function ReactHookForm() {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    const onSubmit = (data) => {
        console.log(data);
    };

    return (
        <div className="container">
            <div className="form-wrapper">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <input
                        type="text"
                        {...register("firstName", {
                            required: "Please enter your first name",
                            minLength: { value: 3, message: "Min length is 3" },
                            pattern: {
                                value: /[A-Za-z]{3}/,
                                message: "Please enter alphabets only",
                            },
                        })}
                        placeholder="First Name"
                        autoComplete="off"
                    />
                    <p>{errors.firstName?.message}</p>
                    <input
                        type="text"
                        {...register("lastName", {
                            required: "Please enter your last name",
                            minLength: { value: 3, message: "Min length is 3" },
                            pattern: {
                                value: /[A-Za-z]{3}/,
                                message: "Please enter alphabets only",
                            },
                        })}
                        placeholder="Last Name"
                        autoComplete="off"
                    />
                    <p>{errors.lastName?.message}</p>
                    <input
                        type="text"
                        {...register("email", {
                            required: "Please enter your email",
                            pattern: {
                                value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                                message: "Please enter a valid email",
                            },
                        })}
                        placeholder="Email"
                        autoComplete="off"
                    />
                    <p>{errors.email?.message}</p>
                    <input
                        type="password"
                        {...register("password", {
                            required: "Please enter your password",
                            pattern: {
                                value: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,14}$/,
                                message:
                                    "Your password should contain at least a smallcase letter, an uppercase letter, a number and a special character and should be between 8 - 14 characters",
                            },
                        })}
                        placeholder="Password"
                        autoComplete="off"
                    />
                    <p>{errors.password?.message}</p>
                    <input
                        className="btn btn-primary"
                        type="submit"
                        value={"Submit"}
                    />
                </form>
            </div>
        </div>
    );
}

export default ReactHookForm;
// /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/
