import React from "react";
import ReactHookForm from "./React_Hook_Form";
import Formik from "./React_Formik";

function App() {
    return (
        <div>
            <h1>Sign Up!</h1>
            <ReactHookForm />
            <Formik />
        </div>
    );
}

export default App;
